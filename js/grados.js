const calcular = document.getElementById('calcular');
const limpiar = document.getElementById('limpiar');

calcular.addEventListener('click', function(){
    let cantidad = document.getElementById('cantidad').value;
    let op = document.getElementById('CaF').checked;
    let resultado = 0;
    console.log(op);
    if(op){
        resultado = (9/5*cantidad)+32;
    }else{
        resultado=(cantidad-32)*5/9;
    }

    document.getElementById('resultado').value=resultado.toFixed(2);
});

limpiar.addEventListener('click', function(){
    document.getElementById('cantidad').value=0;
    document.getElementById('resultado').value=0;
});