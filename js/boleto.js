const calcular = document.getElementById('calcular');
const limpiar = document.getElementById('limpiar');
const enviar = document.getElementById('enviar');

calcular.addEventListener('click', function(){
    let precio = document.getElementById('precio').value;
    let op = document.getElementById('tipo').value
    switch(op){
        case "sencillo":
            let subtotal= precio;
            document.getElementById('subtotal').value=subtotal;
            let impuesto = precio*0.16;
            document.getElementById('impuesto').value=impuesto.toFixed(2);
            let total= precio*1.16;
            document.getElementById('total').value=total.toFixed(2);
            break;
        case "doble":
            let subtotal2 = precio*1.80;
            document.getElementById('subtotal').value=subtotal2;
            let impuesto2 = subtotal2*0.16;
            document.getElementById('impuesto').value=impuesto2.toFixed(2);
            let total2= subtotal2*1.16;
            document.getElementById('total').value=total2.toFixed(2);
            break;
    }
});

limpiar.addEventListener('click', function(){
    document.getElementById('numeroBol').value=" ";
    document.getElementById('nomCliente').value=" ";
    document.getElementById('destino').value=" ";
    document.getElementById('precio').value=0;
    document.getElementById('subtotal').value=0;
    document.getElementById('impuesto').value=0;
    document.getElementById('total').value=0;
});